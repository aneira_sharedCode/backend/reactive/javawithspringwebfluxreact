package com.aneirajave.learn.webflux.repository;

import com.aneirajave.learn.webflux.repository.entity.CustomerEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends ReactiveCrudRepository<CustomerEntity, Long> {

}
