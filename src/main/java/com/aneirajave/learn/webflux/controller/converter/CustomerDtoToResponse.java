package com.aneirajave.learn.webflux.controller.converter;

import com.aneirajave.learn.webflux.controller.dto.CustomerResponse;
import com.aneirajave.learn.webflux.service.CustomerDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerDtoToResponse implements Converter<CustomerDto, CustomerResponse> {

    @Override
    public CustomerResponse convert(CustomerDto source) {
        return CustomerResponse.builder()
                .id(source.getId())
                .name(source.getName())
                .dni(source.getDni())
                .cellPhone(source.getCellPhone())
                .build();
    }
}
