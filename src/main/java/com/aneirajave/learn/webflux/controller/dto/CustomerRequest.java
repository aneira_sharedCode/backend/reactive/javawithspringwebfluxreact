package com.aneirajave.learn.webflux.controller.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerRequest {

    private Long id;
    private String name;
    private String cellPhone;
    private String dni;
}
