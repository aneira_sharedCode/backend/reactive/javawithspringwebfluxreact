package com.aneirajave.learn.webflux.controller;

import com.aneirajave.learn.webflux.controller.dto.CustomerRequest;
import com.aneirajave.learn.webflux.controller.dto.CustomerResponse;
import com.aneirajave.learn.webflux.service.CustomerDto;
import com.aneirajave.learn.webflux.service.CustomerService;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Log4j2
@RestController
@RequestMapping("/customers")
public class CustomerControllerImpl implements CustomerController {

    private final CustomerService customerService;
    private final ConversionService conversionService;

    public CustomerControllerImpl(CustomerService customerService, ConversionService conversionService) {
        this.customerService = customerService;
        this.conversionService = conversionService;
    }

    @GetMapping(value = "", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    @Override
    public Flux<CustomerResponse> getAllCustomerStream() {
        return customerService
                .getAllCustomersStream()
                .mapNotNull(this::convertToResponse);
    }

    @GetMapping(value = "/{id}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    @Override
    public Mono<CustomerResponse> getCustomerById(String id) {
        return null;
    }

    @PostMapping
    @Override
    public Mono<CustomerResponse> createCustomer(CustomerRequest request) {
        log.info("Init createCustomer : {}", request);
        return Mono.just(request)
                .map(this::convertToCustomerDto)
                .flatMap(customerService::saveCustomer)
                .map(this::convertToResponse);
    }

    @PutMapping
    @Override
    public Mono<CustomerResponse> updateCustomer(String id, CustomerRequest request) {
        return null;
    }

    @DeleteMapping
    @Override
    public Mono<Void> deleteCustomer(String id) {
        return null;
    }

    protected CustomerResponse convertToResponse(CustomerDto customerDto) {
        return conversionService.convert(customerDto, CustomerResponse.class);
    }
    protected CustomerDto convertToCustomerDto(CustomerRequest customerRequest) {
        return conversionService.convert(customerRequest, CustomerDto.class);
    }
}
