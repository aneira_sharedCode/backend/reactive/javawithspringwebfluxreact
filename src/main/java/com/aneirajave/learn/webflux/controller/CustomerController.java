package com.aneirajave.learn.webflux.controller;

import com.aneirajave.learn.webflux.controller.dto.CustomerRequest;
import com.aneirajave.learn.webflux.controller.dto.CustomerResponse;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CustomerController {

    Flux<CustomerResponse> getAllCustomerStream();

    Mono<CustomerResponse> getCustomerById(@PathVariable String id);

    Mono<CustomerResponse> createCustomer(@RequestBody CustomerRequest customerRequest);

    Mono<CustomerResponse> updateCustomer(@PathVariable String id, @RequestBody CustomerRequest customerRequest);

    Mono<Void> deleteCustomer(@PathVariable String id);

}
