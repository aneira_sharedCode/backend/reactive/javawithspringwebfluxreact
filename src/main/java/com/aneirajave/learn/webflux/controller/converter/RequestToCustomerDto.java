package com.aneirajave.learn.webflux.controller.converter;

import com.aneirajave.learn.webflux.controller.dto.CustomerRequest;
import com.aneirajave.learn.webflux.service.CustomerDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RequestToCustomerDto implements Converter<CustomerRequest, CustomerDto> {

    @Override
    public CustomerDto convert(CustomerRequest source) {
        return CustomerDto.builder()
                .id(source.getId())
                .name(source.getName())
                .dni(source.getDni())
                .cellPhone(source.getCellPhone())
                .build();
    }
}
