package com.aneirajave.learn.webflux.handler;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;

@Log4j2
@Component
public class LoggerInterceptor implements WebFilter {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        Instant startTime = Instant.now();
        log.info("Init Request: (Method : {}, RequestURI : {})", exchange.getRequest().getMethod(), exchange.getRequest().getURI());

        return chain.filter(exchange).doFinally(signalType -> {
            Instant endTime = Instant.now();
            long executeTime = Duration.between(startTime, endTime).toMillis();
            log.info("End Request: (Method : {}, RequestURI : {}, executeTime : {} ms )", exchange.getRequest().getMethod(), exchange.getRequest().getURI(), executeTime);
        });
    }
}
