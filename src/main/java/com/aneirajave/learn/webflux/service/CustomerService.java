package com.aneirajave.learn.webflux.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CustomerService {

    Flux<CustomerDto> getAllCustomersStream();

    Mono<CustomerDto> getCustomerById(Long id);

    Mono<CustomerDto> saveCustomer(CustomerDto customerDto);

    Mono<CustomerDto> updateCustomer(CustomerDto customerDto);

    Mono<Void> deleteCustomer(Long id);
}