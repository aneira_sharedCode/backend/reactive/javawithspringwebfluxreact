package com.aneirajave.learn.webflux.service;

import com.aneirajave.learn.webflux.repository.CustomerRepository;
import com.aneirajave.learn.webflux.repository.entity.CustomerEntity;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Log4j2
@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final ConversionService conversionService;

    public CustomerServiceImpl(CustomerRepository customerRepository, ConversionService conversionService) {
        this.customerRepository = customerRepository;
        this.conversionService = conversionService;
    }

    public Flux<CustomerDto> getAllCustomersStream() {
        return customerRepository
                .findAll()
                .mapNotNull(this::convertToCustomerDto);
    }

    @Override
    public Mono<CustomerDto> getCustomerById(Long id) {
        return customerRepository
                .findById(id)
                .mapNotNull(this::convertToCustomerDto);
    }

    @Override
    public Mono<CustomerDto> saveCustomer(CustomerDto customerDto) {
        log.info("Init saveCustomer : {}", customerDto);
        return Mono.just(customerDto)
                .map(this::convertToCustomerEntity)
                .flatMap(customerRepository::save)
                .map(this::convertToCustomerDto);
    }

    @Override
    public Mono<CustomerDto> updateCustomer(CustomerDto customerDto) {
        return customerRepository.findById(customerDto.getId())
                .map(entity -> customerDto)
                .map(this::convertToCustomerEntity)
                .flatMap(customerRepository::save)
                .map(this::convertToCustomerDto);
    }

    @Override
    public Mono<Void> deleteCustomer(Long id) {
        return customerRepository.deleteById(id);
    }

    protected CustomerDto convertToCustomerDto(CustomerEntity customerEntity) {
        return conversionService.convert(customerEntity, CustomerDto.class);
    }

    protected CustomerEntity convertToCustomerEntity(CustomerDto customerDto){
        return conversionService.convert(customerDto, CustomerEntity.class);
    }

}
