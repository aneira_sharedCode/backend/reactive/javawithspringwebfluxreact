package com.aneirajave.learn.webflux.service;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerDto {

    private Long id;
    private String name;
    private String cellPhone;
    private String dni;
}
