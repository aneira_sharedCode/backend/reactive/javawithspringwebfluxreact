package com.aneirajave.learn.webflux.service.converter;

import com.aneirajave.learn.webflux.repository.entity.CustomerEntity;
import com.aneirajave.learn.webflux.service.CustomerDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerDtoToCustomerConverter implements Converter<CustomerDto, CustomerEntity> {

    @Override
    public CustomerEntity convert(CustomerDto source) {
        return CustomerEntity.builder()
                .id(source.getId())
                .name(source.getName())
                .dni(source.getDni())
                .cellPhone(source.getCellPhone())
                .build();
    }
}
