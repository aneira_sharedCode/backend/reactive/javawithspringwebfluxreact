package com.aneirajave.learn.webflux.service.converter;

import com.aneirajave.learn.webflux.repository.entity.CustomerEntity;
import com.aneirajave.learn.webflux.service.CustomerDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerToCustomerDtoConverter implements Converter<CustomerEntity, CustomerDto> {

    @Override
    public CustomerDto convert(CustomerEntity source) {
        return CustomerDto.builder()
                .name(source.getName())
                .dni(source.getDni())
                .cellPhone(source.getCellPhone())
                .build();
    }
}
