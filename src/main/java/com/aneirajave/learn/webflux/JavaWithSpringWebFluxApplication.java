package com.aneirajave.learn.webflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

@EnableR2dbcRepositories
@SpringBootApplication
public class JavaWithSpringWebFluxApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaWithSpringWebFluxApplication.class, args);
    }

}
